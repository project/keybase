Keybase
=======

Download and extract the module to the usual path, then enable it via the
modules page.

Make sure you have 'Administer site configuration' permission.

Run `keybase prove web http://example.com' and then visit the keybase module
configuration page at admin/config/services/keybase

Paste the generated well-known content from keybase (the ==== lines and 
everything in between) into the settings form and click 'Save configuration'.

Check that you can see the generated content at http://example.com/keybase.txt

Tell keybase it can check the web content.

Done!
